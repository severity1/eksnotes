
# Setup your local workspace

## Configure your AWS Credentials

Run the `aws configure` command.

```bash
aws configure
```

> NOTE: You must consider that some AWS account setups require MFA and role switching.

Verify that configuration was successful.

```bash
aws sts get-caller-identity
```

> NOTE: This is a good way of validating if your worskpace can access AWS.