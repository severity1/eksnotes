# Create EKS Cluster

## Create new EKS Cluster

Create the cluster.

```bash
eksctl create cluster --region=us-east-1 --node-type=t2.medium
```

> NOTE: The `eksctl create cluster` creates a default vpc for
> your eks cluster but it also allows you to pass arguments to manipulate
> the default vpc creation behavior, you can also use an existing vpc or
> you can pass a yaml config file tht defines the cluster make up.
> In most cases, you will want to provision your VPC separate from EKS
> and using your Infrastructure as code tooling ie; CloudFormation,
> Terraform, Pulumi, etc. See example below;
>
> ```yaml
>
> apiVersion: eksctl.io/v1alpha5
> kind: ClusterConfig
>
> metadata:
>   name: cluster-in-existing-vpc
>   region: eu-north-1
>
> vpc:
>   subnets:
>     private:
>       eu-north-1a: {id: subnet-0ff156e0c4a6d300c}
>       eu-north-1b: {id: subnet-0549cdab573695c03}
>       eu-north-1c: {id: subnet-0426fb4a607393184}
>
> nodeGroups:
>    - name: ng-1-api1
>      labels: {role: api}
>      instanceType: m5.xlarge
>      desiredCapacity: 10
>      privateNetworking: true
>    - name: ng-1-api2
>      labels: {role: api}
>      instanceType: m5.xlarge
>      desiredCapacity: 10
>      privateNetworking: true
>    - name: ng-1-worker
>      labels: {role: workers}
>      instanceType: m5.xlarge
>      desiredCapacity: 10
>      privateNetworking: true
>    - name: ng-2-builders
>      labels: {role: builders}
>      instanceType: m5.2xlarge
>      desiredCapacity: 2
>      privateNetworking: true
>      iam:
>        withAddonPolicies:
>          imageBuilder: true
> ```

## Connect to the new EKS Cluster

Verify that the cluster configuration works.

```bash
kubectl get nodes
```