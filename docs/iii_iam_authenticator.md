# IAM Authenticator

By default the creator of the cluster is the only one who can do whatever
inside the kubernetes cluster. I, as the creator can give people with IAM
credentials access to our EKS Cluster with varying degrees of permissiveness.

As the creator I can edit the `configmap/aws-auth`.

```bash
kubectl edit -n kube-system configmap/aws-auth
```

Let's add Vladislav by him under the `mapUsers` section.

```yaml
apiVersion: v1
data:
  mapRoles: |
    - rolearn: arn:aws:iam:xxxxxxx:role:mycluster-worker-node-InstanceProfile-xYz
      username: system:node:{{ECPrivateDNName}}
      groups:
        - system:bootstrappers
        - system:nodes
  mapUsers: |
    - userarn: arn:aws:iam:xxxxxxx:user/vladislav
      username: vladislav
      groups:
        - system:masters
kind: ConfigMap
...
```

Next, Let's add a role used to assume in the target account under the `mapRoles` section.

```yaml
apiVersion: v1
data:
  mapRoles: |
    - rolearn: arn:aws:iam:xxxxxxx:role:mycluster-worker-node-InstanceProfile-xYz
      username: system:node:{{ECPrivateDNName}}
      groups:
        - system:bootstrappers
        - system:nodes
    - rolearn: arn:aws:iam::xxxxxxx:role/TestRole
      username: TestRole
      groups:
        - system:masters
  mapUsers: |
    - userarn: arn:aws:iam:xxxxxxx:user/vladislav
      username: vladislav
      groups:
        - system:masters
kind: ConfigMap
...
```

> NOTE: Seems atm, this is the difinitive way to map iam user to EKS rbac.
> One thing to figure out is a way to manage this.