# Deploying an Application to EKS

Here we just deploy a basic app manually.

> Ideally all apps to be deployed in EKS will have a `k8s/` directory
> containing the `deployment.yaml` and `service.yaml`, `deployment.yaml`
> defines your pod/s to be deployed into the nodes while `service.yaml`
> defines the service abstraction ie; associates the pod/s deployed from
> `deployment.yaml` to loadbalancers.

## Clone project

Clone the repository.

```bash
git clone https://github.com/linuxacademy/eks-deep-dive-2019.git
```

Change to the `eks-deep-dive-2019/2-LA1-Deploying-an-Application-to-EKS` directory.

```bash
cd eks-deep-dive-2019/2-LA1-Deploying-an-Application-to-EKS
```

## Create the Deployment

Examine the `deployment.yaml` file.

```bash
cat deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 1
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
        ports:
        - containerPort: 80
```

Create the deployment.

```bash
kubectl apply -f deployment.yaml
```

Check the status of the deployment.

```bash
kubectl get deployment nginx
```

## Create the Service

```bash
cat service.yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx
  labels:
    app: nginx
spec:
  selector:  
    app: nginx
  type: LoadBalancer
  ports:
  - port: 80
    protocol: TCP
```

## Browse to the ELB's Public DNS Name

Check the status of the service.

```bash
kubectl get service nginx -o wide
```

Test the connectivity.

```bash
kubectl get service nginx -o json
```

Retrieve the DNS name of the ELB.

```bash
ELB=$(kubectl get service nginx -o json | jq -r '.status.loadBalancer.ingress[].hostname`)
echo $ELB
```

Run a curl request.

```bash
curl -m3 -v $ELB
```

Undeploy the application.

```bash
kubectl delete -f service.yaml
kubectl delete -f deployment.yaml
```

> NOTE: Ideally the steps mentioned above should be part of a publish step
> in your CI/CD pipeline, ie;
>
> - `docker build`
> - `docker push`
> - `docker tag`
> - `kubectl apply -f deployment.yaml`
> - `kubectl apply -f service.yaml`
>
> Though, we need to figure out what kubectl commands to run to just update existing pod.