# Autoscaling

There are 2 types of autoscaling in EKS and both are required in production.

The first one is called Horizontal Pod Autoscaler (HPA) and this is mainly
used to autoscale deployed pods based on Kubernetes' Metric Server API. For example;
We can tell kubernetes to scale pod count when CPU reaches 50%.

The second one is Cluster Autoscaler (CA) which autoscales both pods and ec2 instances
inside the autoscaling group.