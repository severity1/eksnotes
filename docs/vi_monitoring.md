# Monitoring

Most popular solution to use in EKS are;
- Cloudwatch for EC2 and AWS Services used.
- Prometheus for collecting data on your EKS Cluster but mostly for operational metrics.
- Grafana for visualizing.