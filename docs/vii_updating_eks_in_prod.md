# Updating EKS in Production

## Updating Kubernetes versions

EKS let's you update the control plane easily, the control plane includes the masters and the etcd servers.

```bash
aws eks update-cluster-version --name <clustername> --kubernetes-version 1.11
```

Next is update the Kube network proxy to match the control plane version.
Seems you need to define it as a yaml object.

```yaml
spec:
  template:
    spec:
      containers:
        - name: kube-proxy
          image: xxxxxxxxx.dkr.ecr.us-west-2.amazonaws.com/eks/kube-proxy:v1.11.5
```

Then we patch our kube-proxy.

```bash
kubectl patch daemonset kube-proxy -n kube-system --patch "$(cat kube-proxy-patch.yaml)"
```

Next is to upgrade the nodegroup, apparently the most ideal way to do this is spinup new nodegroups
migrate pods to the new nodegroup and delete the old nodegroup.

If you are on a kubernetes version lower that 1.11 you can optionally replace KubeDNS with CoreDNS
which apparently performs better.